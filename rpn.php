<?php

require_once "stack.php";


class RpnEvaluator
{
	const DEFAULT_STACK_SIZE = Stack::DEFAULT_STACK_SIZE;
	const OPERAND_NONE = 0;
	const OPERAND_READ = 1;
	const OPERAND_INVALID = 2;

	private $stack;
	private $result;
	private $error_status = "";


	function __construct($stack_size = self::DEFAULT_STACK_SIZE)
	{
		$this->stack = new Stack($stack_size);
	}


	public function evaluate($expression)
	{
		$len = strlen($expression);
		for ($i = 0; $i < $len; $i += 1) {
			$read_result = $this->read_operand($expression, $i, $len);
			switch ($read_result) {
			case self::OPERAND_INVALID:
				return;
			case self::OPERAND_READ:
				if ($this->check_stack_errors()) {
					return;
				}
				if ($i >= $len) {
					return $this->end_evaluation();
				}
				break;
			default:
				// OPERAND_NONE
				break;
			}
			$ch = $expression[$i];
			switch ($ch) {
			case " ":
				continue 2;
			case "=":
				return $this->end_evaluation();
			case "+":
			case "-":
			case "*":
			case "/":
				$this->operator($ch);
				if ($this->check_stack_errors() || $this->error_status !== "") {
					return;
				}
				break;
			default:
				$this->error_status = "bad character at position $i";
				return;
			}
		}
		return $this->end_evaluation();
	}


	public function retrieve_result()
	{
		return $this->result;
	}


	public function get_error()
	{
		return $this->error_status;
	}


	public function get_stack_top()
	{
		return $this->stack->get_top();
	}


	private function read_operand($expression, &$i, $len)
	{
		$operand = "";
		$decimal_point = false;
		for (; $i < $len; $i += 1) {
			$ch = $expression[$i];
			if ($ch >= "0" && $ch <= "9") {
				$operand .= $ch;
			}
			elseif ($ch == "." && $decimal_point == false) {
				$operand .= $ch;
				$decimal_point = true;
			}
			else {
				break;
			}
		}
		if ($operand === "") {
			return self::OPERAND_NONE;
		}
		if ($operand === "." || $operand[-1] == ".") {
			$this->error_status = "bad character at position $i";
			return self::OPERAND_INVALID;
		}
		$this->stack->push($operand);
		return self::OPERAND_READ;
	}


	private function operator($ch)
	{
		$operand1 = $this->stack->pop();
		$operand2 = $this->stack->pop();
		switch ($ch) {
		case "+": $this->stack->push($operand2 + $operand1); break;
		case "-": $this->stack->push($operand2 - $operand1); break;
		case "*": $this->stack->push($operand2 * $operand1); break;
		case "/":
			if ($operand1 == 0 || is_infinite($operand1)) {
				$this->error_status = "invalid division";
				return;
			}
			$this->stack->push($operand2 / $operand1);
			break;
		}
	}


	private function check_stack_errors()
	{
		if (($stack_error = $this->stack->get_error_status()) !== "") {
			$this->error_status = "stack " . $stack_error;
			return true;
		}
		return false;
	}


	private function end_evaluation()
	{
		$this->result = $this->stack->pop();
		$this->check_stack_errors();
		return $this->result;
	}
}
