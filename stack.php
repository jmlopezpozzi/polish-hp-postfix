<?php

class Stack
{
	const DEFAULT_STACK_SIZE = 64;

	private $stack_size;
	private $top = 0;
	private $contents = array();
	private $error_status = "";


	function __construct($size = self::DEFAULT_STACK_SIZE)
	{
		$this->stack_size = $size;
	}


	public function push($i)
	{
		if ($this->error_status !== "") {
			return;
		}
		if ($this->top >= $this->stack_size) {
			// Stack overflow
			$this->error_status = "overflow";
			return;
		}
		$this->contents[$this->top] = $i;
		$this->top += 1;
	}


	public function pop()
	{
		if ($this->error_status !== "") {
			return NULL;
		}
		if ($this->top <= 0) {
			// Stack underflow
			$this->error_status = "underflow";
			return NULL;
		}
		$this->top -= 1;
		$result = $this->contents[$this->top];
		unset($this->contents[$this->top]);
		return $result;
	}


	public function get_top()
	{
		return $this->top;
	}


	public function get_error_status()
	{
		return $this->error_status;
	}
}
