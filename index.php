<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
</head>
<body>
	<form action="" method="get">
		<p>Enter RPN expression:</p>
		<input type="text" name="expression">
		<button type="submit">EVALUATE</button>
	</form>
	<?php
	$expression = isset($_GET["expression"]) ?
	              htmlspecialchars($_GET["expression"]) :
	              NULL;
	if (is_null($expression)) {
		return;
	}
	require_once "rpn.php";
	echo "<p>Expression is $expression</p>";
	$rpn = new RpnEvaluator();
	$result = $rpn->evaluate($expression);
	if ($result !== NULL) {
		echo "<p>Result is $result</p>";
	}
	else {
		echo "<p>Error: {$rpn->get_error()}</p>";
	}
	if ($rpn->get_stack_top() != 0) {
		echo "<p>Stack is not empty</p>";
	}
	?>
</body>
</html>
